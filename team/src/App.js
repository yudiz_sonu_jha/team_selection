import './App.css';
import React, { useEffect,useState, createContext ,useContext} from 'react'
import axios from 'axios';
import Icon, { HomeOutlined } from '@ant-design/icons';
import TotalPlayer from './TotalPlayer'

import Player from './Player';

function App() {
 
  const [post,setPost] = useState([]);
  const [totalSelected,setTotalSelected] = useState(0);
  const [kkr, setKkr] = useState(0);
    const [rr, setRr] = useState(0);

  useEffect(() => {
    axios.get("https://apiv2.gullysport.com/api/user/match-player/626fa00111bcc9296d8a0e5e/v2").then((response) => {
      response.data.data.matchPlayer.map(v=>
        v.isSelected=false
      )
    setPost(response.data.data);
    console.log(response.data.data)
    });
  }, []);

  function toggle(id){
    const newMatchPlayer = post.matchPlayer.map(v=> {
                            if(v._id === id){
                              v.isSelected = !v.isSelected;
                            }
                            return v;
                          });
                          post.matchPlayer = newMatchPlayer;
                          setPost(JSON.parse(JSON.stringify(post)));
  }
  

  return (
    <div className="App">
      <div className='player'>
        <div className='header'>
            <HomeOutlined style={{marginLeft:"50px",marginTop:"20px"}}/>
            <p style={{margin:"auto",marginLeft:"80px",marginTop:"-20px",fontSize:"12px"}}>KKR VS RR</p>
            <p style={{margin:"auto",marginLeft:"80px",marginTop:"-3px",fontSize:"12px"}}>May 2,2022 7:30PM</p>
            <p style={{margin:"auto",textAlign:"center",fontSize:"12px",marginTop:"17px"}}>Maximum 7 Sum players from a team</p>

            <div className='player_score' style={{display:"flex"}}>
                <div style={{height:"40px",width:"20%",marginLeft:"20px",marginTop:"-10px"}}>
                    <h5>Player {totalSelected}</h5>
                </div>

                 <div style={{height:"40px",width:"20%",marginLeft:"20px",marginTop:"-10px"}}>
                    <h5>KKR {kkr}</h5>
                </div>

                 <div style={{height:"40px",width:"20%",marginLeft:"20px",marginTop:"-10px"}}>
                    <h5>RR {rr}</h5>
                </div>

                 <div style={{height:"40px",width:"20%",marginLeft:"20px",marginTop:"-10px"}}>
                    <h5>Credits Left</h5>
                </div>
            </div>
        </div> 
        
        
              <TotalPlayer player={post.matchPlayer} setTotalSelected={setTotalSelected} setKkr={setKkr} setRr={setRr} />
            
              <div className='posthdr2' style={{height:"30px",width:"100%",backgroundColor:"white",border:"1px solid grey",margin:"auto",textAlign:"center",marginTop:"-9px",}}>
                  <p style={{margin:"auto",}}>Select</p>
              </div>

             

            <div className='posts' style={{overflowY:"scroll" , height:"400px"}}>
                    {
                      (post && post.matchPlayer  && post.matchPlayer.map(v=> <Player key={v._id} v={v} toggle={toggle} totalSelected={totalSelected} /> ) )
                    }
                   
                
            </div>
        
    </div>
    
    </div>
  );
}

export default App;