import React from 'react'
import './App.css'

function Player({v,toggle, totalSelected}) {

 
  return (
    
      <div className='pname' style={{display:"flex",justifyContent:"space-between"}}>
          <div style={{height:"18px",width:"100%",margin:"auto"}}>
            <h4>{v.sName}</h4>
          </div>

            <div style={{height:"18px",width:"100%",margin:"auto",textAlign:"center"}}>
            <h4>{v.nSeasonPoints}</h4>
          </div>
          
          <div style={{height:"18px",width:"100%",margin:"auto",textAlign:"center"}}>
              <h5>{v.nFantasyCredit}</h5>
          </div>
          {!v.isSelected && <button disabled={totalSelected>=11} onClick={() => toggle(v._id)}>+</button>}
          {v.isSelected && <button onClick={() => toggle(v._id)}>-</button>}
          
      </div> 
  )
}
  

export default Player