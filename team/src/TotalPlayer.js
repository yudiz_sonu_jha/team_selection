import React, {useEffect, useState} from 'react'

const TotalPlayer = ({player, setTotalSelected,setKkr,setRr}) => {
    const [wk, setWk] = useState(0);
    const [bl, setBl] = useState(0);
    const [bt, setBt] = useState(0);
    const [al, setAl] = useState(0);
    
    
    useEffect(()=>{
        if(!player) return ;
        let bolwer = 0;
        let batsman = 0;
        let allrounder= 0;
        let weketkeeper = 0;
        let kkr = 0;
        let rr = 0;
        let ts = 0;

        player.map(val=>{
            if(val.isSelected){
                ts++;
                
                if(val.eRole === 'BWL'){
                    bolwer = bolwer+1;
                  }
                  if(val.eRole === 'BATS'){
                    batsman = batsman+1;
                  }
                  if(val.eRole === 'WK'){
                    allrounder = allrounder+1;
                  }
                  if(val.eRole === 'ALLR'){
                    weketkeeper =weketkeeper+1;
                  }
                  if(val.oTeam.sShortName === 'KKR'){
                    kkr =kkr+1;
                  }
                  if(val.oTeam.sShortName === 'RR'){
                    rr =rr+1;
                  }
            }
            
        });

        setWk(weketkeeper);
        setBl(bolwer);
        setAl(allrounder);
        setBt(batsman);
        setTotalSelected(ts);
        setRr(rr);
        setKkr(kkr);
        setTotalSelected(ts);
    },[player])

  return (
    <div className='post_header' style={{height:"34px",width:"100%",display:"flex",justifyContent:"space-between"}}>
          
        <div className='' style={{marginLeft:"10px",fontSize:"12px",marginTop:"5px"}}>
        <button >Wk {wk}</button>
        </div>

        <div className='' style={{marginLeft:"10px",fontSize:"12px",marginTop:"5px"}}>
        <button>ALLR {al}</button>
        </div>

        <div className='' style={{marginLeft:"10px",fontSize:"12px",marginTop:"5px"}}>
        <button>BATS {bt}</button>
        </div>

        <div className='' style={{marginLeft:"10px",fontSize:"12px",marginTop:"5px"}}>
        <button>BWLS {bl}</button>
        </div>  
    </div>
  );
}

export default TotalPlayer


